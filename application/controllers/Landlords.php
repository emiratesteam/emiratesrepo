<?php 
defined('BASEPATH') OR exit('No direct script allowed');
   class Landlords extends CI_Controller {
	  protected $baseFolder		=	'front-end';
	  protected $baseFolder2	=	'admin/landlord';
	  protected $table			=	'landlord';
	  protected $header			=	'front-end/header.php';
	  protected $footer			=	'front-end/footer.php';
	  protected $header2		=	'admin/header.php';
	  protected $footer2		=	'admin/footer.php';
      public function __construct() { 
         parent::__construct(); 
        $this->load->library('session');      
    	$this->load->model(array('Landlords_model'));
        $this->load->model(array('LandlordCategory_model'));
        //for email function  
        $this->load->library('email');
              
      } 
   
     public function index() { 
        $query = $this->db->get("$this->table"); 
        $data['records'] = $query->result(); 
        $this->load->helper('url'); 
        $this->load->view("$this->header");
        $data['landlord_categories'] = $this->LandlordCategory_model->fetchAll();
        $this->load->view("$this->baseFolder/landlords",$data);
        $this->load->view("$this->footer");
     }  
   	public function adminIndex() { 
       
        if (@$_SESSION['logged_in']) {
        $num_rows=$this->db->count_all("$this->table");
     	$this->load->library('pagination');

		$config['base_url'] = base_url().'index.php/Landlords/index';
		$config['total_rows'] = $num_rows;
		$config['per_page'] = 5;
		
		//$config['use_page_numbers'] = TRUE;
		$config['full_tag_open'] = "<ul class='pagination'>";
		$config['full_tag_close'] ="</ul>";
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
		$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
		$config['next_tag_open'] = "<li>";
		$config['next_tagl_close'] = "</li>";
		$config['prev_tag_open'] = "<li>";
		$config['prev_tagl_close'] = "</li>";
		$config['first_tag_open'] = "<li>";
		$config['first_tagl_close'] = "</li>";
		$config['last_tag_open'] = "<li>";
		$config['last_tagl_close'] = "</li>";
		
		$this->pagination->initialize($config);
     	// ******* pagiantion configrtn ends *** /////////
     
        //$query = $this->db->get("$this->table",$config['per_page'],$this->uri->segment(3)); // $config['per_page'] and $offset is for pagination
        //$data['records'] = $query->result(); 
        $data['records'] = $this->Landlords_model->getAllData($config['per_page'],$this->uri->segment(3));
        $this->load->view("$this->header2");
        $this->load->view("$this->baseFolder2/index",$data);
        //$this->load->view('home/home_view',$data);
        $this->load->view("$this->footer2");
        } else {
            redirect(base_url().'index.php/AdminLogin');
        }
     }  
	 public function add()
	 { 
     	$property_name			= NULL;
     	$property_category_id	= NULL; 
     	$location				= NULL; 
     	$name					= NULL;
     	$contact_no		    	= NULL;
     	$available_date		    = NULL;  
     	$email		    		= NULL;
     	$askingPrice		    = NULL;  
     	$description		    = NULL;  	
     	$submit 				= NULL;
     	
     	extract($_POST);
     	$params['property_name']		=	$property_name;
     	$params['property_category_id']	=	$property_category_id;
     	$params['location']	 			=	$location;
     	$params['name']	 				=	$name;    
     	$params['contact_no']	 		=	$contact_no;
     	$params['available_date']	 	=	date('Y-m-d', strtotime($available_date));  
     	$params['email']	 			=	$email;        
     	$params['askingPrice']	 		=	$askingPrice; 
     	$params['description']	 		=	$description;  
     	
     	//Check whether user upload picture
            $this->load->library('upload');
            $fileUpload=array();$isUpload=FALSE;
            $property_image=array(
            	'upload_path'=>'./uploads/',
            	'allowed_types'=>'jpg|jpeg|png|gif',
            	'encrypt_name'=>TRUE
            );
          $this->upload->initialize($property_image);
          if($this->upload->do_upload('property_image')){
		  	$fileUpload=$this->upload->data();
		  	$isUpload=TRUE;
		  	 $params['image_url']  	=	$fileUpload['file_name'];
		  } 
     	 // echo $fileUpload['file_name'];die; 
     	
     	if(isset($submit))
     	{		
			$res=$this->Landlords_model->insertData($params);			
			 if($res)
	         {
	         	/*if($res2) {
					//for mail function
			     	$subject = "Sahara Branch Login Credentials";
			     	$msg = "UserName:".$userName."\n"."Password:".$password;
			     	mail($email,$subject,$msg);
				}
	         	*/
	         	 $this->session->set_flashdata("flash", ["type" => "success", "message" => "Data added successfully!"]);
			 }
			 else{
			 	 $this->session->set_flashdata("flash", ["type" => "danger", "message" => "Failed to add data!"]);
			 }
		}
		 redirect('Landlords/index');		
	 }
       public function delete() { 
         $id = $this->uri->segment('3'); 
         $res= $this->Landlords_model->rowWiseData($id);     	
		 $img= $res->image_url;			       		
         $res2=$this->Landlords_model->deleteData($id); 
         if($res2)
         {
         	 $url='uploads/'.$img;
			 if (file_exists($url)) {
		        unlink($url);
		       }
		 	$this->session->set_flashdata("flash", ["type" => "success", "message" => "Data deleted successfully!"]);
		 }
		 else{
		 	 $this->session->set_flashdata("flash", ["type" => "danger", "message" => "Failed to delete data!"]);
		 }
        redirect('Landlords/adminIndex');   		
      }     	 
  } 
