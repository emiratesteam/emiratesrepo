<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Dashboard
            <small>Control panel</small>
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-lg-12"><h4>Welcome to Emirates admin panel</h4></div>
        </div>
        <p></p>
        <?php
/*        var_dump($_SESSION);
        */?>
        
        <div class="row">
            <div class="col-lg-5 col-xs-5">
                <!-- small box -->
                <div class="small-box">
                    <div class="inner">   
                        <h4>Property Category</h4></br></br>
                    </div>
                    <div class="icon">
                        <i class="fa fa-object-group"></i>
                    </div>
                    <a href="<?= base_url(); ?>index.php/PropertyCategory/" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <div class="col-lg-5 col-xs-5">
                <!-- small box -->
                <div class="small-box">
                    <div class="inner">
                        <h4>Properties</h4></br></br>
                    </div>
                    <div class="icon">
                        <i class="fa fa-cube"></i>
                    </div>
                    <a href="<?= base_url(); ?>index.php/AdminProperties" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
         </div>
          <div class="row">
            <div class="col-lg-5 col-xs-5">
                <!-- small box -->
                <div class="small-box">
                    <div class="inner">
                        <h4>Landlords Category</h4></br></br>
                    </div>
                    <div class="icon">
                        <i class="fa fa-arrows-alt"></i>
                    </div>
                    <a href="<?= base_url(); ?>index.php/LandlordCategory/" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>            
            <div class="col-lg-5 col-xs-5">
                <!-- small box -->
                <div class="small-box">
                    <div class="inner">
                        <h4>Landlords</h4></br></br>
                    </div>
                    <div class="icon">
                        <i class="fa fa-dropbox"></i>
                    </div>
                    <a href="<?= base_url(); ?>index.php/Landlords/adminIndex" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
         </div>
         <div class="row">
            <div class="col-lg-5 col-xs-5">
                <!-- small box -->
                <div class="small-box">
                    <div class="inner">
                        <h4>Enquiries</h4></br></br>
                    </div>
                    <div class="icon">
                        <i class="fa fa-search"></i>
                    </div>
                    <a href="<?php echo base_url(); ?>index.php/Contact/getData" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>                        
        </div>
    </section>
</div>