<?php
defined('BASEPATH') OR exit('No direct script allowed');
   class Contact extends CI_Controller {
	  protected $baseFolder		=	'front-end';
	  protected $baseFolderAdmin=	'admin';
	  protected $table			=	'enquiries';
	  protected $header			=	'front-end/header.php';
	  protected $footer			=	'front-end/footer.php';
	  
	  protected $header2		=	'admin/header.php';
	  protected $footer2		=	'admin/footer.php';
	  
      public function __construct() { 
         parent::__construct();       
    	$this->load->model(array('Contact_model'));
        //for email function  
        $this->load->library('session');
        $this->load->library('email');
        $this->load->helper('url');    
      } 
   
     public function index() {        
        $this->load->view("$this->header");
        $this->load->view("$this->baseFolder/contact");
        $this->load->view("$this->footer");
     }  
    public function getData() {           
        $num_rows=$this->db->count_all("$this->table");
     	$this->load->library('pagination');

		$config['base_url'] = base_url().'index.php/Contact/getData';
		$config['total_rows'] = $num_rows;
		$config['per_page'] = 5;
		
		//$config['use_page_numbers'] = TRUE;
		$config['full_tag_open'] = "<ul class='pagination'>";
		$config['full_tag_close'] ="</ul>";
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
		$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
		$config['next_tag_open'] = "<li>";
		$config['next_tagl_close'] = "</li>";
		$config['prev_tag_open'] = "<li>";
		$config['prev_tagl_close'] = "</li>";
		$config['first_tag_open'] = "<li>";
		$config['first_tagl_close'] = "</li>";
		$config['last_tag_open'] = "<li>";
		$config['last_tagl_close'] = "</li>";
		
		$this->pagination->initialize($config);
     	// ******* pagiantion configrtn ends *** /////////
     $this->db->order_by('enquiries.ID','desc');
        $query = $this->db->get("$this->table",$config['per_page'],$this->uri->segment(3)); // $config['per_page'] and $offset is for pagination
        $data['records'] = $query->result(); 
        
        $this->load->view("$this->header2");
        $this->load->view("$this->baseFolderAdmin/contacts/index",$data);
        //$this->load->view('home/home_view',$data);
        $this->load->view("$this->footer2");
        
     } 
	 public function add()
	 {
	 	$name					= NULL;
     	$email					= NULL;
     	$phone					= NULL;
     	$subject		    	= NULL; 
     	$message		    	= NULL;  
     	 	
     	$submit 				= NULL;
     	
     	extract($_POST);
     	$params['name']	 				=	$name;     
     	$params['email']				=	$email;
     	$params['phone']	 			=	$phone;   
     	$params['subject']	 			=	$subject; 
     	$params['message']				=	$message;
     	     	     	
     	if(isset($submit))
     	{		
			$res=$this->Contact_model->insertData($params);			
			 if($res)
	         {
	         	/*if($res2) {
					//for mail function
			     	$subject = "Sahara Branch Login Credentials";
			     	$msg = "UserName:".$userName."\n"."Password:".$password;
			     	mail($email,$subject,$msg);
				}
	         	*/
	         	 $this->session->set_flashdata("flash", ["type" => "success", "message" => "Enquiry sent successfully!"]);
			 }
			 else{
			 	 $this->session->set_flashdata("flash", ["type" => "danger", "message" => "Failed to sent data!"]);
			 }
		}
		 redirect('Contact/index');		
	 }	
   public function delete() { 
         $id = $this->uri->segment('3'); 
         $res=$this->Contact_model->deleteData($id); 
         if($res)
         {
		 	$this->session->set_flashdata("flash", ["type" => "success", "message" => "Data deleted successfully!"]);
		 }
		 else{
		 	 $this->session->set_flashdata("flash", ["type" => "danger", "message" => "Failed to delete data!"]);
		 }
        redirect('Contact/getData');   		
      } 
  } 
