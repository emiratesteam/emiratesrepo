<div class="page_head">
    <div class="page_head_inner">
        <div class="container">
            <h4>Contact Us</h4>
        </div>
    </div>
</div>
<div class="contact_page_wrap">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="contact_head text-capitalize">get in touch with us</h2>
                <div class="contact_head_bg"><span></span></div>
                <p class="contact_head_cap">We are happy to here from you.</p>
            </div>
        </div>
        <div class="row" style="margin-top: 50px;">
            <div class="col-lg-4 col-md-4 col-sm-4">
                <div class="contact_item clearfix">
                    <div class="pull-left">
                        <span class="ico text-center"><i class="ion ion-location"></i></span>
                    </div>
                    <div class="desc">
                        <h4>Address: </h4>
                        <p>Al Ghurair Centre<br />Office Tower, Room No. 839<br/>P.O.Box 376304, Dubai, U.A.E</p>
                    </div>
                </div>
                <div class="contact_item clearfix">
                    <div class="pull-left">
                        <span class="ico text-center"><i class="ion ion-android-call"></i></span>
                    </div>
                    <div class="desc">
                        <h4>Phone: </h4>
                        <p><a href="tel:+97142955588">+971 4 295 5588</a><br /><a href="tel:+97142955405">+971 4 295 5405</a></p>
                    </div>
                </div>
                <div class="contact_item clearfix">
                    <div class="pull-left">
                        <span class="ico text-center"><i class="ion ion-android-mail"></i></span>
                    </div>
                    <div class="desc">
                        <h4>Mail: </h4>
                        <p><a href="mail:info@emirates.com">info@emirates.com</a><br /><a href="mail:contact@emirates.com">contact@emirates.com</a></p>
                    </div>
                </div>
            </div>
            <div class="col-lg-8 col-md-8 col-sm-8">
            <?php
			        if ($this->session->flashdata('flash')) {
			            ?>
			            <div class="status status-<?= $this->session->flashdata('flash')['type']; ?>" data-role="auto-hide">
			                <?= $this->session->flashdata('flash')['message']; ?>
			            </div>
			            <?php
			        }
			        ?>
                <form action="<?php echo site_url(); ?>/Contact/add" class="contact_form" method="post">
                    <div class="row">
                     
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <div class="form_block">
                                <label>Name* <span></span></label>
                                <input type="text" name="name">
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <div class="form_block">
                                <label>Email* <span></span></label>
                                <input type="email" name="email">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <div class="form_block">
                                <label>Phone</label>
                                <input type="text" name="phone">
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <div class="form_block">
                                <label>Subject</label>
                                <input type="text" name="subject">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form_block">
                                <label>Message* <span></span></label>
                                <textarea name="message"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form_block">
                                <button class="text-uppercase" id="trigger_lanlord" type="submit" name="submit"><strong>Submit Now</strong></button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div id="map">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3608.1065116924724!2d55.31598707908842!3d25.267002200781935!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0000000000000000%3A0xd87c839b0da90455!2sAl+Ghurair+Centre!5e0!3m2!1sen!2sae!4v1460550297308" frameborder="0"></iframe>
    </div>
</div>