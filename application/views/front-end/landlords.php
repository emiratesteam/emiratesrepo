<div class="page_head">
    <div class="page_head_inner">
        <div class="container">
            <h4>List your Properties</h4>
        </div>
    </div>
</div>
<div class="landlord_page_wrap">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-4">
                <div class="landlord_aside">
                    <h3>List your Properties</h3>
                    <h4>Emirates Properties Working For You...</h4>
                    <p>Thinking of selling/renting property? Advertise your property with Emirates Properties, one of most trusted and recognized real estate companies of Dubai. Give us your property details by filling out the form below. Our agents will get back to you to arrange the next step.</p>
                </div>
            </div>
            <div class="col-lg-8 col-md-8 col-sm-8">
            <?php
			        if ($this->session->flashdata('flash')) {
			            ?>
			            <div class="status status-<?= $this->session->flashdata('flash')['type']; ?>" data-role="auto-hide">
			                <?= $this->session->flashdata('flash')['message']; ?>
			            </div>
			            <?php
			        }
			        ?>
            	<form action="<?php echo site_url(); ?>/landlords/add" class="landlord_form" enctype="multipart/form-data" method="post">              
		           <div class="row">
		            
			          
		               <div class="col-lg-6 col-md-6 col-sm-6">
		                <div class="form_block">
		                    <label>Property Name <span></span></label>
		                    <input type="text" name="property_name" >
		                </div>
		               </div>
		               <div class="col-lg-6 col-md-6 col-sm-6">
			           	<div class="form_block">
                            <label>Property Category <span></span></label>
                                <select name="property_category_id" >
                                    <option value="">Select</option>
                                    <?php
                                    foreach ($landlord_categories as $landlord_category) {
                                        ?>
                                        <option value="<?= $landlord_category['id'] ?>"><?= $landlord_category['landlordCategory']; ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                        	</div>
                        </div>
		           	</div>
                   <div class="row">
                   	   <div class="col-lg-6 col-md-6 col-sm-6">
		                 <div class="form_block">
                                <label>Location* <span></span></label>
                                <input type="text" name="location" required>
                            </div>
		               </div>
			           <div class="col-lg-6 col-md-6 col-sm-6">
			                <div class="form_block">
			                    <label>Asking Price <span></span></label>
	                            <input type="text" name="askingPrice" >	          
			                </div>
		               </div>                       
		           </div>
		          
		           <div class="row">
		            	<div class="col-lg-6 col-md-6 col-sm-6">
				           	<div class="form_block">
	                            <label>Name* <span></span></label>
	                            <input type="text" name="name" required>
	                        </div>
			           </div>
			           <div class="col-lg-6 col-md-6 col-sm-6">
			           	<div class="form_block">
                                <label>Contact No <span></span></label>
                                <input type="text" name="contact_no" >
                            </div>
			           </div>
			        </div>
		           <div class="row">
		           		<div class="col-lg-6 col-md-6 col-sm-6">
			           	<div class="form_block">
                            <label>Email* <span></span></label>
                            <input type="email" name="email" required>
                        </div>
			           </div>
		               <div class="col-lg-6 col-md-6 col-sm-6">
		                <div class="form_block">
                            <label>Available Date <span></span></label>
                            <input type="text" name="available_date" data-role="date_picker"  class="datepicker">
                        </div>
		               </div>		          			           
			       </div>
		           <div class="row">
		               
		               <div class="col-lg-6 col-md-6 col-sm-6">
                            <div class="form_block">
                                <label>Property Image (768x576)</label>
                                <input type="file" name="property_image" data-action="show_thumbnail">
                            </div>
                        </div>
		           </div>
		           
		           
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form_block">
                                <label>Description*</label>
                                <textarea name="description" required></textarea>
                            </div>
                        </div>
                    </div>
                   
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form_block">
                                <button class="text-uppercase" id="trigger_lanlord" type="submit" name="submit"><strong>Submit Now</strong></button>
                            </div>
                        </div>
                    </div>
                </div>
                </form>
            </div>
        </div>

    </div>
</div>