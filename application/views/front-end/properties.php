<div class="page_head">
    <div class="page_head_inner">
        <div class="container">
            <h4>Our Properties</h4>
        </div>
    </div>
</div>
<div class="property_page_wrap">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-4 col-sm-4">
                <ul class="property_filter list-unstyled">
                	<li><a <?php if($this->uri->segment(4)=='') { ?> class="active"<?php }?> href="<?= site_url(); ?>/Properties/index/0/0">All</a></li>
                <?php
                if (count($property_categories) > 0) { 
                    foreach ($property_categories as $categories) {                    	
                ?>
                   <li><a <?php if($this->uri->segment(4)==$categories['id']) { ?> class="active"<?php }?> href="<?= site_url(); ?>/Properties/index/0/<?= $categories['id']; ?>"><?= $categories['property_category']; ?></a></li> 
                <?php
                	}
                }
                ?>
                   <!-- <li><a href="javascript:void(0)">Apartments</a></li>
                    <li><a href="javascript:void(0)">Houses</a></li>
                    <li><a href="javascript:void(0)">Villa</a></li>
                    <li><a href="javascript:void(0)">Flat</a></li>
                    <li><a href="javascript:void(0)">Garages</a></li>
                    <li><a href="javascript:void(0)">Commercial</a></li>-->
                </ul>
            </div>
            <div class="col-lg-9 col-md-8 col-sm-8">
                <div class="row">                    
                    <?php
                    $count=0;
                    $totCount=count($records);
                    if (count($records) > 0) {                    	
                        $i =$this->uri->segment(3);
                        foreach ($records as $property) {
                        	++$count;  $i++;                    	
                            ?>
                     <div class="col-lg-4 col-md-4 col-sm-4">
                        <div class="property_box">
                            <a href="<?= base_url(); ?>index.php/contact" class="property_img">
                            <?php if($property->image_url){
							?>
							<img src="<?php echo base_url();?>/uploads/<?php echo $property->image_url; ?>" class="img-responsive" alt="" />                                      
							<?php
						}
                          else{
						  	?>
						  	<img src="<?php echo base_url();?>/uploads/images.jpg" class="img-responsive" alt="" /> 
							<?php
						  }
						  ?>                            
                            </a>
                            <div class="property_title">
                                <h4><?= $property->property_name; ?></h4>
                                <div class="property_loc">
                                    <span class="ico"><i class="ion ion-location"></i></span>
                                    <span><?= $property->location; ?></span>
                                </div>
                            </div>
                            <div class="property_det">
                                <ul class="list-unstyled">
                                    <!--<li class="clearfix">
                                        <span class="pull-left">Area</span>
                                        <span class="pull-right"><?= $property->area; ?></span>
                                    </li>-->
                                    <li class="clearfix">
                                        <span class="pull-left">Category</span>
                                        <span class="pull-right"><?= $property->property_category; ?></span>
                                    </li>
                                    <li class="clearfix">
                                        <!--<span class="pull-left">Location</span>-->
                                        <p><?= $property->description; ?></p>
                                    </li>
                                   
                                </ul>
                            </div>
                        </div>
                    </div>
                        <?php
                        if($count%3==0)
                        	{
								?>
								</div>
								<div class="row">
								<?php
							}
						 }
                       }?>                         
                    </div>
                </div>
                <?php 
                  	$rowCount = count($records);
                  	 ?>
                  	<!-- for pagination --->
					<div class="row" align="center">
						<?php if($rowCount!=0) { echo $this->pagination->create_links();} else {}; ?>
					</div>
					<!-- pagination end -->
            </div>
        </div>
    </div>
</div>