-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: May 31, 2017 at 05:10 AM
-- Server version: 5.5.54-cll-lve
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bodhiinf_emirates`
--

-- --------------------------------------------------------

--
-- Table structure for table `enquiries`
--

CREATE TABLE `enquiries` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `subject` text NOT NULL,
  `message` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `enquiries`
--

INSERT INTO `enquiries` (`id`, `name`, `email`, `phone`, `subject`, `message`) VALUES
(1, 'Anu', 'anu@gmail.com', '9876655432', 'House for rent', 'Need one 2bhk house for rent');

-- --------------------------------------------------------

--
-- Table structure for table `landlord`
--

CREATE TABLE `landlord` (
  `id` int(11) NOT NULL,
  `property_name` varchar(100) NOT NULL DEFAULT '0',
  `property_category_id` int(11) NOT NULL DEFAULT '0',
  `location` varchar(100) NOT NULL,
  `image_url` varchar(100) DEFAULT '0',
  `name` varchar(100) DEFAULT '0',
  `contact_no` varchar(100) DEFAULT '0',
  `email` varchar(100) DEFAULT '0',
  `available_date` date DEFAULT NULL,
  `description` text,
  `askingPrice` varchar(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `landlord`
--

INSERT INTO `landlord` (`id`, `property_name`, `property_category_id`, `location`, `image_url`, `name`, `contact_no`, `email`, `available_date`, `description`, `askingPrice`) VALUES
(1, 'Moozhikkunnu', 3, 'Kannur', 'dfea9ac983ae21a2992eeee5e909b9b4.jpg', 'anu', '12345', 'anu@gmail.com', '2017-03-30', 'place for resort', '2500000'),
(2, 'dongle', 4, 'Tvm', '0', 'Anas', '98766778987', 'anu@gmail.com', '2017-03-30', 'dongle for commetial purpose with 10000sqrt land and 2000 sqrt 4 building', '100000'),
(5, 'ds', 2, 'ds', '0', 'sd', 'sdv', 'sdv@fgdfh.v', '1970-01-01', 'sdvsdv', 'svdvsd'),
(6, 'mirsad', 2, 'bombe', '6df961bc922ff5325b86b9538c15b70b.jpg', 'asif', '12345', 'akhila.m8@gmail.ocm', '2017-02-26', 'pool', '1212');

-- --------------------------------------------------------

--
-- Table structure for table `landlord_category`
--

CREATE TABLE `landlord_category` (
  `id` int(11) NOT NULL,
  `landlordCategory` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `landlord_category`
--

INSERT INTO `landlord_category` (`id`, `landlordCategory`) VALUES
(1, 'Villa'),
(2, 'House'),
(3, 'Land'),
(4, 'Office'),
(5, 'Shops'),
(6, 'Apartments'),
(7, 'Flats'),
(8, 'Commercial');

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE `login` (
  `id` int(11) NOT NULL,
  `userType` varchar(20) NOT NULL,
  `userName` varchar(20) NOT NULL,
  `password` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`id`, `userType`, `userName`, `password`) VALUES
(1, 'admin', 'admin', '$2y$10$LqLLN/0/0eCRgwGuycXPP..iTd94JiJAcSlQ6lCdXAjO2GHc.9SeK');

-- --------------------------------------------------------

--
-- Table structure for table `properties`
--

CREATE TABLE `properties` (
  `id` int(11) NOT NULL,
  `property_name` varchar(100) NOT NULL DEFAULT '0',
  `property_category_id` int(11) NOT NULL DEFAULT '0',
  `location` varchar(100) NOT NULL,
  `image_url` varchar(100) DEFAULT '0',
  `description` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `properties`
--

INSERT INTO `properties` (`id`, `property_name`, `property_category_id`, `location`, `image_url`, `description`) VALUES
(1, 'Millad', 2, 'Calicut', 'a6e92a4c4f3bace5521f195a66e6e6ca.jpg', 'Building for any commercial applications..'),
(2, 'Nihal', 1, 'Kannur', '7499f713bf9d6bdaf1c8b258de859206.jpg', 'House with 2bhk'),
(3, 'Harani', 1, 'Calicut', '8bb2c828c7c5a58d7d080ca44531bd06.jpg', 'Land for residents');

-- --------------------------------------------------------

--
-- Table structure for table `property_category`
--

CREATE TABLE `property_category` (
  `id` int(11) NOT NULL,
  `property_category` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `property_category`
--

INSERT INTO `property_category` (`id`, `property_category`) VALUES
(1, 'Residential buildings'),
(2, 'Commercial buildings'),
(3, 'Villas');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `enquiries`
--
ALTER TABLE `enquiries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `landlord`
--
ALTER TABLE `landlord`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `landlord_category`
--
ALTER TABLE `landlord_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `login`
--
ALTER TABLE `login`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `properties`
--
ALTER TABLE `properties`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `property_category`
--
ALTER TABLE `property_category`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `enquiries`
--
ALTER TABLE `enquiries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `landlord`
--
ALTER TABLE `landlord`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `landlord_category`
--
ALTER TABLE `landlord_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `login`
--
ALTER TABLE `login`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `properties`
--
ALTER TABLE `properties`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `property_category`
--
ALTER TABLE `property_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
