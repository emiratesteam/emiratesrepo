<?php 
   class Landlords_model extends CI_Model {
	protected $table='landlord';
      function __construct() { 
         parent::__construct(); 
         $this->load->database();
      } 
   	 public function fetchAll() {
        $query = $this->db->get($this->table);
        return $query->result_array();
    }
   	public function insertData($params)
   	{ 
		$ins	=	$this->db->insert($this->table,$params);
		return $ins;
	}
	 public function getAllData($limit,$offset){
		    $this->db->select('landlord.*,landlord_category.*,landlord.id as land_id,landlord_category.id as cat_id');
			$this->db->from('landlord');
			$this->db->join('landlord_category','landlord.property_category_id = landlord_category.id');		
			$this->db->order_by('landlord.id','desc');
            $this->db->limit($limit, $offset);
			$query = $this->db->get();
			//echo $this->db->last_query();die;
			return $query->result();
	    }
 public function deleteData($id) { 
         if ($this->db->delete('landlord', "id = ".$id)) { 
            return true; 
         } 
      } 
       public function rowWiseData($editId)
      {
	  	$query 	= $this->db->query("SELECT image_url FROM landlord where id='$editId'");
		$row 	= $query->row();
		//echo $this->db->last_query();die;
		return $row;
	  }	   
} 

