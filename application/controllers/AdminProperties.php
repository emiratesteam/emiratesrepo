<?php 
defined('BASEPATH') OR exit('No direct script allowed');
   class AdminProperties extends CI_Controller {
	  protected $baseFolder		=	'admin/properties';
	  protected $table			=	'properties';
	  protected $header			=	'admin/header.php';
	  protected $footer			=	'admin/footer.php';
	  
      public function __construct() { 
         parent::__construct();       
    	$this->load->model(array('Properties_model'));
        $this->load->model(array('PropertyCategory_model'));
        $this->load->library('session');
        if(empty($this->session->userdata("user_id")))
        {
        	$this->session->set_flashdata("flash",["type"=>"danger","message"=>"Session out!"]);
        	redirect('AdminLogin/index');
        }
      } 
     public function index() { 
       /* $data['records']			 = $this->Properties_model->getAllData();
        $this->load->helper('url'); 
        $this->load->view("$this->header");
        $this->load->view("$this->baseFolder/index",$data);
        $this->load->view("$this->footer");*/
        if (@$_SESSION['logged_in']) {
        $num_rows=$this->db->count_all("$this->table");
     	$this->load->library('pagination');

		$config['base_url'] = base_url().'index.php/AdminProperties/index';
		$config['total_rows'] = $num_rows;
		$config['per_page'] = 5;
		
		//$config['use_page_numbers'] = TRUE;
		$config['full_tag_open'] = "<ul class='pagination'>";
		$config['full_tag_close'] ="</ul>";
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
		$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
		$config['next_tag_open'] = "<li>";
		$config['next_tagl_close'] = "</li>";
		$config['prev_tag_open'] = "<li>";
		$config['prev_tagl_close'] = "</li>";
		$config['first_tag_open'] = "<li>";
		$config['first_tagl_close'] = "</li>";
		$config['last_tag_open'] = "<li>";
		$config['last_tagl_close'] = "</li>";
		
		$this->pagination->initialize($config);
     	// ******* pagiantion configrtn ends *** /////////
     
        //$query = $this->db->get("$this->table",$config['per_page'],$this->uri->segment(3)); // $config['per_page'] and $offset is for pagination
        //$data['records'] = $query->result(); 
        $data['records'] = $this->Properties_model->getAllData($config['per_page'],$this->uri->segment(3));
        $this->load->view("$this->header");
        $this->load->view("$this->baseFolder/index",$data);
        //$this->load->view('home/home_view',$data);
        $this->load->view("$this->footer");
        } else {
            redirect(base_url().'index.php/AdminLogin');
        }
     }  
    public function add_view(){
	 	$this->load->helper('url'); 
	 	$data['property_categories'] = $this->PropertyCategory_model->fetchAll();
        $this->load->view("$this->header");
        $this->load->view("$this->baseFolder/add",$data);
        $this->load->view("$this->footer");
	 }	
	 public function add()
	 {
	 	$property_name			= NULL;
     	$property_category_id	= NULL;
     	$location		    	= NULL; 
     	$description		    = NULL;    	
     	$submit 				= NULL;
     	
     	extract($_POST);
     	$params['property_name']		=	$property_name;    
     	$params['property_category_id']	=	$property_category_id;
     	$params['location']	 			=	$location;   
     	$params['description']	 		=	$description;
     	//Check whether user upload picture
            $this->load->library('upload');
            $fileUpload=array();$isUpload=FALSE;
            $property_image=array(
            	'upload_path'=>'./uploads/',
            	'allowed_types'=>'jpg|jpeg|png|gif',
            	'encrypt_name'=>TRUE
            );
          $this->upload->initialize($property_image);
          if($this->upload->do_upload('property_image')){
		  	$fileUpload=$this->upload->data();
		  	$isUpload=TRUE;
		  	$params['image_url']  	=	$fileUpload['file_name'];
		  } 
     	 // echo $fileUpload['file_name'];die; 
     	 
     	if(isset($submit))
     	{		
			$res=$this->Properties_model->insertData($params);			
			 if($res)
	         {
	         	/*if($res2) {
					//for mail function
			     	$subject = "Sahara Branch Login Credentials";
			     	$msg = "UserName:".$userName."\n"."Password:".$password;
			     	mail($email,$subject,$msg);
				}
	         	*/
	         	 $this->session->set_flashdata("flash", ["type" => "success", "message" => "Data added successfully!"]);
			 }
			 else{
			 	 $this->session->set_flashdata("flash", ["type" => "danger", "message" => "Failed to add data!"]);
			 }
		}
		 redirect('AdminProperties/index');		
	 }
	 
	 
	 public function edit_view()
	 {
	 	$data['fields']=array(
		'ID',
		'property_name',
		'property_category_id',
		'location',
		'description'
		);
	 	
	 	 $this->load->helper('form'); 
         $tableId = $this->uri->segment('3'); 
         $data['condition'] = array(
         'ID'=>$tableId
         );  
         	    
         $data['results']=$this->Properties_model->getUpdateData($data);
         $data['property_categories'] = $this->PropertyCategory_model->fetchAll();
         $this->load->view("$this->header");
         $this->load->view("$this->baseFolder/edit",$data);
         $this->load->view("$this->footer");
	 }
	  public function edit()
	 {	 
	 	$editId					= NULL;
	 	$property_name			= NULL;
     	$property_category_id	= NULL;
     	$location		    	= NULL; 
     	$description		    = NULL;
     	$changeImage    		= NULL;
     	$submit 				= NULL;
     	
     	extract($_POST);
     	$editId				 			=	$editId;
     	$params['property_name']		=	$property_name;   
     	$params['property_category_id']	=	$property_category_id;
     	$params['location']	 			=	$location;   
     	$params['description']	 		=	$description;  
     	if($changeImage=='yes')
     	{     					
			//Check whether user upload picture
            $this->load->library('upload');
            $fileUpload=array();$isUpload=FALSE;
            $property_image=array(
            	'upload_path'=>'./uploads/',
            	'allowed_types'=>'jpg|jpeg|png|gif',
            	'encrypt_name'=>TRUE
            );
            $this->upload->initialize($property_image);
            if($this->upload->do_upload('property_image')){
	            $fileUpload=$this->upload->data();
			  	$isUpload=TRUE;
			  	if($isUpload=='TRUE')
			  	{
					$res=$this->Properties_model->rowWiseData($editId);
		     		if (isset($res))
					{
				        $img= $res->image_url;
				        $url='uploads/'.$img;
						if (file_exists($url)) {
					        unlink($url);
					       }
					     $params['image_url']  	=	$fileUpload['file_name'];  
					}
				}		  	 
			}  	      
		}
     	     	     	
     	if(isset($submit))
     	{			
			$res=$this->Properties_model->updateAction($params,$editId);
			 if($res)
	         {
			 	 $this->session->set_flashdata("flash", ["type" => "success", "message" => "Data updated successfully!"]);
			 }
			 else{
			 	 $this->session->set_flashdata("flash", ["type" => "danger", "message" => "Failed to update data!"]);
			 }
		}		
        redirect('AdminProperties/index');
	 }
  	public function delete() { 
         $id = $this->uri->segment('3'); 
         $res=$this->Properties_model->rowWiseData($id);     	
		 $img= $res->image_url;			       		
         $res2=$this->Properties_model->deleteData($id); 
         if($res2)
         {
         	 $url='uploads/'.$img;
			 if (file_exists($url)) {
		        unlink($url);
		       }
		 	$this->session->set_flashdata("flash", ["type" => "success", "message" => "Data deleted successfully!"]);
		 }
		 else{
		 	 $this->session->set_flashdata("flash", ["type" => "danger", "message" => "Failed to delete data!"]);
		 }
        redirect('AdminProperties/index');   		
      }        
   } 
