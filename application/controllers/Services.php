<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Services extends CI_Controller {

    public function index()
    {
        $this->load->helper('url');
        $this->load->view('front-end/header.php');
        $this->load->view('front-end/services.php');
        $this->load->view('front-end/footer.php');
    }
}
