<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Properties extends CI_Controller {
	  protected $baseFolder		=	'front-end/properties';
	  protected $table			=	'properties';
	  protected $header			=	'front-end/header.php';
	  protected $footer			=	'front-end/footer.php';
	  
	 public function __construct() { 
         parent::__construct();       
    	$this->load->model(array('Properties_model'));
        $this->load->model(array('PropertyCategory_model'));
        $this->load->helper('url');
        $this->load->library('session');
      } 
    public function index()
    {
    	$catId = $this->uri->segment(4); 
        /*$data['records'] = $this->Properties_model->getAllData();
        $this->load->view('front-end/header.php');
        $this->load->view('front-end/properties.php',$data);
        $this->load->view('front-end/footer.php');*/
        
        $num_rows=$this->db->count_all("$this->table");
     	$this->load->library('pagination');

		$config['base_url'] = base_url().'index.php/Properties/index';
		$config['total_rows'] = $num_rows;
		$config['per_page'] = 15;
		
		//$config['use_page_numbers'] = TRUE;
		$config['full_tag_open'] = "<ul class='pagination'>";
		$config['full_tag_close'] ="</ul>";
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
		$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
		$config['next_tag_open'] = "<li>";
		$config['next_tagl_close'] = "</li>";
		$config['prev_tag_open'] = "<li>";
		$config['prev_tagl_close'] = "</li>";
		$config['first_tag_open'] = "<li>";
		$config['first_tagl_close'] = "</li>";
		$config['last_tag_open'] = "<li>";
		$config['last_tagl_close'] = "</li>";
		
		$this->pagination->initialize($config);
     	// ******* pagiantion configrtn ends *** /////////
     
        //$query = $this->db->get("$this->table",$config['per_page'],$this->uri->segment(3)); // $config['per_page'] and $offset is for pagination
        //$data['records'] = $query->result(); 
        if($catId)
			{
        $data['records'] = $this->Properties_model->getAllDataCat($config['per_page'],$this->uri->segment(3),$catId);
        }
        else{
			$data['records'] = $this->Properties_model->getAllData($config['per_page'],$this->uri->segment(3));
		}
        $data['property_categories'] = $this->PropertyCategory_model->fetchAll();
        $this->load->view("$this->header");
        $this->load->view("$this->baseFolder",$data);
        //$this->load->view('home/home_view',$data);
        $this->load->view("$this->footer");
    }
   
}
