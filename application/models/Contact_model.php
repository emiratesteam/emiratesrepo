<?php 
   class Contact_model extends CI_Model {
	protected $table='enquiries';
      function __construct() { 
         parent::__construct(); 
         $this->load->database();
      } 
   	 public function fetchAll() {
        $query = $this->db->get($this->table);
        return $query->result_array();
    }
   	public function insertData($params)
   	{ 
		$ins	=	$this->db->insert($this->table,$params);
		return $ins;
	}
        public function deleteData($id) { 
	     if ($this->db->delete('enquiries', "id = ".$id)) { 
	        return true; 
	     } 
	  } 
} 
