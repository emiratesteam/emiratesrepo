<!DOCTYPE HTML>
<html lang="">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Emirates</title>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>css/ionicons.min.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>plugins/datepicker/datepicker3.css">
    <link rel="stylesheet" type="text/css" href="css/jquery.bxslider.min.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>css/style.css">
</head>
<body>
<nav class="responsive_nav">
    <ul class="list-unstyled">
        <li><a href="<?= base_url(); ?>" <?= (!$this->uri->segment(1)) ? 'class="active"' : ''; ?>>Home</a></li>
        <li><a href="<?= base_url(); ?>index.php/services" <?= ($this->uri->segment(1) == 'services') ? 'class="active"' : ''; ?>>Our Services</a></li>
        <li><a href="<?= base_url(); ?>index.php/properties" <?= ($this->uri->segment(1) == 'properties') ? 'class="active"' : ''; ?>>Our Properties</a></li>
        <li><a href="<?= base_url(); ?>index.php/landlords" <?= ($this->uri->segment(1) == 'landlords') ? 'class="active"' : ''; ?>>List your Properties</a></li>
        <li><a href="<?= base_url(); ?>index.php/contact" <?= ($this->uri->segment(1) == 'contact') ? 'class="active"' : ''; ?>>Contact Us</a></li>
    </ul>
</nav>
<header>
    <div class="container">
        <div class="header_inner clearfix">
            <a class="logo" href="<?= base_url(); ?>"><img class="img-responsive" src="<?= base_url(); ?>images/logo.png" alt="Emirates"></a>
            <nav class="header_nav">
                <ul class="clearfix">
                    <li><a href="<?= base_url(); ?>" <?= (!$this->uri->segment(1)) ? 'class="active"' : ''; ?>>Home</a></li>
                    <li><a href="<?= base_url(); ?>index.php/services" <?= ($this->uri->segment(1) == 'services') ? 'class="active"' : ''; ?>>Our Services</a></li>
                    <li><a href="<?= base_url(); ?>index.php/properties" <?= ($this->uri->segment(1) == 'properties') ? 'class="active"' : ''; ?>>Our Properties</a></li>
                    <li><a href="<?= base_url(); ?>index.php/landlords" <?= ($this->uri->segment(1) == 'landlords') ? 'class="active"' : ''; ?>>List your Properties</a></li>
                    <li><a href="<?= base_url(); ?>index.php/contact" <?= ($this->uri->segment(1) == 'contact') ? 'class="active"' : ''; ?>>Contact Us</a></li>
                </ul>
            </nav>
            <span class="nav_trigger"><i class="ion ion-navicon"></i></span>
        </div>
    </div>
</header>