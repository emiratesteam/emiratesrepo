<?php
defined('BASEPATH') OR exit('No direct script access allowed');
Class PropertyCategory_model extends CI_Model {
    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->library('session');
        $this->load->helper('url');
    }
    public function fetchAll() {
        $query = $this->db->get('property_category');
        return $query->result_array();
    }
    public function create() {
        $data['property_category'] = $this->input->post('property_category');
        return $this->db->insert('property_category', $data);
    }
    public function getItemById($id) {
        $query = $this->db->get_where('property_category', array('id' => $id));
        return $query->row_array();
    }
    public function update_item($id) {
        $data['property_category'] = $this->input->post('property_category');
        return $this->db->update('property_category', $data, array('id' => $id));
    }
     public function deleteData($id) { 
         if ($this->db->delete('property_category', "id = ".$id)) { 
            return true; 
         } 
      } 
}