<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Property
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-lg-8">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Add New Property</h3>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-lg-4 col-md-5 col-sm-5">
                                <form action="<?php echo site_url(); ?>/AdminProperties/add" method="post" enctype="multipart/form-data">
                                    <div class="form-group">
                                        <label for="property_name">Property Name</label><span class="text-danger">*</span>
                                        <input type="text" name="property_name" id="property_name" class="form-control" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="property_category_id">Property Category</label><span class="text-danger">*</span>
                                        <select name="property_category_id" id="property_category_id" class="form-control" required>
                                            <option value="">Select</option>
                                            <?php
                                            foreach ($property_categories as $property_category) {
                                                ?>
                                                <option value="<?= $property_category['id'] ?>"><?= $property_category['property_category']; ?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="location">Location</label><span class="text-danger">*</span>
                                        <input type="text" name="location" id="location" class="form-control" required>
                                    </div>
                                     <div class="form-group">
                                        <label for="description">Description</label><span class="text-danger">*</span>
                                        <textarea name="description" id="description" class="form-control" ></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="property_image">Property Image (768x576)</label>
                                        <input type="file" name="property_image" data-action="show_thumbnail">
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary btn-flat" name="submit">Save</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- /.content-wrapper -->
