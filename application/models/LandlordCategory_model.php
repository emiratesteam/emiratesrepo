<?php
defined('BASEPATH') OR exit('No direct script access allowed');
Class LandlordCategory_model extends CI_Model {
    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->library('session');
        $this->load->helper('url');
    }
    public function fetchAll() {
        $query = $this->db->get('landlord_category');
        return $query->result_array();
    }
    public function create() {
        $data['landlordCategory'] = $this->input->post('landlordCategory');
        return $this->db->insert('landlord_category', $data);
    }
    public function getItemById($id) {
        $query = $this->db->get_where('landlord_category', array('id' => $id));
        return $query->row_array();
    }
    public function update_item($id) {
        $data['landlordCategory'] = $this->input->post('landlordCategory');
        return $this->db->update('landlord_category', $data, array('id' => $id));
    }
     public function deleteData($id) { 
         if ($this->db->delete('landlord_category', "id = ".$id)) { 
            return true; 
         } 
      } 
}