<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Land-lords
        </h1>
    </section>
    <section class="content">
        <?php
        if ($this->session->flashdata('flash')) {
            ?>
            <div class="status status-<?= $this->session->flashdata('flash')['type']; ?>" data-role="auto-hide">
                <?= $this->session->flashdata('flash')['message']; ?>
            </div>
            <?php
        }
        ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="box box-primary">                   
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Property Name</th>
                                <th>Category</th>
                                <th>Location</th>
                                <th>Image</th>                               
                                <th style="width: 200px">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            if (count($records) > 0) {
                            	$i =$this->uri->segment(3); 
                                foreach ($records as $property) {
                                	$i++;
                                    ?>
                                    <tr>
                                        <td><?= $i; ?></td>
                                        <td><?= $property->property_name; ?></td>
                                        <td><?= $property->landlordCategory; ?></td>
                                        <td><?= $property->location; ?></td>
                                        <?php if($property->image_url){
											?>
											<td><img src="<?php echo base_url();?>/uploads/<?php echo $property->image_url; ?>"></td>                                      
											<?php
										}
                                          else{
										  	?>
											<td><img src="<?php echo base_url();?>/uploads/defaultImage.jpg"></td>                                      
											<?php
										  }
										  ?>                                    
                                        <td>
                                            <a href="#" data-toggle="modal" data-target="#myModal<?php echo $property->land_id; ?>" class="btn btn-success btn-flat">View</a>  
                                            <a href="<?php echo site_url(); ?>/Landlords/delete/<?= $property->land_id; ?>" onclick="return delete_type()" class="btn btn-danger btn-flat" onclick="return delete_type()">Delete</a>                                          
                                            <!-- Modal pop up end -->
                                            <div id="myModal<?php echo $property->land_id; ?>" class="modal fade" role="dialog">
											  <div class="modal-dialog">

											    <!-- Modal content-->
											   
											    <div class="modal-content">
											      <div class="modal-header bg-blue">
											        <button type="button" class="close" data-dismiss="modal">&times;</button>
											        <h4 class="modal-title">Land-lord details</h4>
											      </div>
											      
											      <div class="modal-body">
							
							<table class="table table-bordered" align="center" width="100%">
								<tr>
									<th>Property Name</th>
									<th>:</th>
									<th><?php if($property->property_name){  echo $property->property_name; }?></th>
								</tr>
								<tr>
									<th>Category</th>
									<th>:</th>
									<th><?php  if($property->landlordCategory){ echo $property->landlordCategory; }?></th>
								</tr>
								<tr>
									<th>Location</th>
									<th>:</th>
									<th><?php if($property->location){  echo $property->location;} ?></th>
								</tr>	
								<tr>
									<th>Customer Name</th>
									<th>:</th>
									<th><?php if($property->name){ echo $property->name;} ?></th>
								</tr>
								<tr>
									<th>Contact No.</th>
									<th>:</th>
									<th><?php  if($property->contact_no){ echo $property->contact_no; }?></th>
								</tr>
								<tr>
									<th>Email</th>
									<th>:</th>
									<th><?php if($property->email){ echo $property->email;} ?></th>
								</tr>	
								
								<tr>
									<th>Date</th>
									<th>:</th>
									<th><?php if($property->available_date){ echo $property->available_date; }?></th>
								</tr>	
								<tr>
									<th>Description</th>
									<th>:</th>
									<th><?php if($property->description){ echo $property->description;} ?></th>
								</tr>
								<tr>
									<th>Asking Price </th>
									<th>:</th>
									<th><?php if($property->askingPrice){ echo $property->askingPrice;} ?></th>
								</tr>
								
								<?php if($property->image_url){
										?>
									<tr>
										<th>Image</th>
										<th>:</th>
										<th><img src="<?php echo base_url();?>/uploads/<?php echo $property->image_url; ?>"></th>          
									</tr>                            
									<?php
										}                                      
									?>    												
							</table>
											      </div>
											      
											      
											      <div class="box-body table-responsive no-padding">
						
											    </div>
												</form>
											  </div>
											</div>
										</div>
											<!-- Modal pop up end -->
                                        </td>
                                    </tr>
                                    <?php                                    
                                }
                            } else {
                                ?>
                                <tr><td colspan="9" align="center">No records found.</td></tr>
                                <?php
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                 <?php 
                  	$rowCount = count($records);
                  	 ?>
                  	<!-- for pagination --->
					<div class="row" align="center">
						<?php if($rowCount!=0) { echo $this->pagination->create_links();} else {}; ?>
					</div>
					<!-- pagination end -->
            </div>
        </div>
    </section>
</div>
<script>	
function delete_type()
{
var del=confirm("Do you Want to Delete ?");
	if(del==true)
	{
	window.submit();
	}
	else
	{
	return false;
	}
}

function change_approve()
{
var del=confirm("Do you want to approve..?");
	if(del==true)
	{
	window.submit();
	}
	else
	{
	return false;
	}
}
function change_block()
{
var del=confirm("Do you want to block..?");
	if(del==true)
	{
	window.submit();
	}
	else
	{
	return false;
	}
}
function change_unblock()
{
var del=confirm("Do you Want to unblock..?");
	if(del==true)
	{
	window.submit();
	}
	else
	{
	return false;
	}
}
</script>
<!-- /.content-wrapper -->
