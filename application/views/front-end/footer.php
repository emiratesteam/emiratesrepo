<footer>
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-3 footer_logo">
                <a href="<?= base_url(); ?>"><img class="img-responsive" src="<?= base_url(); ?>images/logo.png" alt="Emirates"></a>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-4 col-lg-offset-1 col-md-offset-1">
                <p class="footer_address">Al Ghurair Centre, Office Tower<br />Suite Number. 839 P.O.Box 376304<br />Dubai, U.A.E<br />Phone No     <a href="tel:+97142955588">+971 4 295 5588</a>&nbsp;&nbsp;&nbsp;<br />fax  <a href="tel:+97142955405">+971 4 295 5405</a><br /><a href="email:Info@emiratesproperties.com">Info@emiratesproperties.com</a></p>
            </div>
            <div class="col-lg-2 col-md-3 col-sm-3 col-lg-offset-1 col-md-offset-1">
                <ul class="footer_nav list-unstyled">
                    <li><a href="<?= base_url(); ?>" class="active">Home</a></li>
                    <li><a href="<?= base_url(); ?>index.php/services">Our Services</a></li>
                    <li><a href="<?= base_url(); ?>index.php/properties">Our Properties</a></li>
                    <li><a href="<?= base_url(); ?>index.php/landlords">List your Properties</a></li>
                    <li><a href="<?= base_url(); ?>index.php/contact">Contact Us</a></li>
                </ul>
            </div>
            <div class="col-lg-1 col-md-1 col-sm-1">
                <div class="footer_soc">
                    <a href="#" class="fb"><i class="ion ion-social-facebook"></i></a>
                    <a href="#" class="tw"><i class="ion ion-social-twitter"></i></a>
                    <!--<a href="#" class="in"><i class="ion ion-social-instagram"></i></a>-->
                    <a href="#" class="li"><i class="ion ion-social-linkedin"></i></a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="clearfix powered_wrap">
                    <div class="copy_rt">&copy; <script>document.write(new Date().getFullYear());</script> Emirates Properties, All Right reserved</div>
                    <div class="powered">Powered by &nbsp;&nbsp;<a href="http://bodhiinfo.com" target="_blank"><img src="<?= base_url(); ?>images/bodhi_logo.png" alt="Bodhi"></a></div>
                </div>
            </div>
        </div>
    </div>
</footer>

<script src="<?= base_url(); ?>js/jquery-2.2.4.min.js"></script>
<script src="<?= base_url(); ?>js/bootstrap.min.js"></script>
<script src="<?= base_url(); ?>plugins/datepicker/bootstrap-datepicker.js"></script>

<script src="<?= base_url(); ?>js/particles.min.js"></script>
<script src="<?= base_url(); ?>js/jquery.bxslider.js"></script>
<script src="<?= base_url(); ?>js/script.js"></script>
  <script>
    $(document).ready(function(){
        $('.bxslider').bxSlider({
        pager: false,
        controls: false,
        mode: 'fade',
        pause: 7000,
        auto: true
})
    });
  </script>

<script src="<?= base_url(); ?>js/stats.js"></script>
<script>
    $(function () {
        if ($('#particles-js').length > 0) {
            // Particle JS
            particlesJS("particles-js", {
                    "particles": {
                        "number": {
                            "value":100, "density": {
                                "enable": true, "value_area": 1200
                            }
                        }
                        , "color": {
                            "value": "#ffffff"
                        }
                        , "shape": {
                            "type":"circle", "stroke": {
                                "width": 0, "color": "#ffffff"
                            }
                            , "polygon": {
                                "nb_sides": 5
                            }
                            , "image": {
                                "src": "img/github.svg", "width": 100, "height": 100
                            }
                        }
                        , "opacity": {
                            "value":0.5, "random":false, "anim": {
                                "enable": false, "speed": 0.3, "opacity_min": 0.1, "sync": false
                            }
                        }
                        , "size": {
                            "value":3, "random":true, "anim": {
                                "enable": false, "speed": 40, "size_min": 0.1, "sync": false
                            }
                        }
                        , "line_linked": {
                            "enable": true, "distance": 150, "color": "#ffffff", "opacity": 0.4, "width": 1
                        }
                        , "move": {
                            "enable":true, "speed":6, "direction":"none", "random":false, "straight":false, "out_mode":"out", "bounce":false, "attract": {
                                "enable": false, "rotateX": 600, "rotateY": 1200
                            }
                        }
                    }
                    , "interactivity": {
                        "detect_on":"canvas", "events": {
                            "onhover": {
                                "enable": true, "mode": "repulse"
                            }
                            , "onclick": {
                                "enable": true, "mode": "push"
                            }
                            , "resize":true
                        }
                        , "modes": {
                            "grab": {
                                "distance":400, "line_linked": {
                                    "opacity": 1
                                }
                            }
                            , "bubble": {
                                "distance": 400, "size": 40, "duration": 0.5, "opacity": 8, "speed": 1
                            }
                            , "repulse": {
                                "distance": 200, "duration": 0.1
                            }
                            , "push": {
                                "particles_nb": 4
                            }
                            , "remove": {
                                "particles_nb": 2
                            }
                        }
                    }
                    , "retina_detect":false
                }

            );
        }
    });
</script>
</body>
</html>