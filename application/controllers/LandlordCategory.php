<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LandlordCategory extends CI_Controller {
	protected $baseFolder		=	'admin/landlord-category';
	  protected $table			=	'landlord_category';
	 protected $header			=	'admin/header.php';
	  protected $footer			=	'admin/footer.php';
	public function __construct() { 
         parent::__construct();      
         
        $this->load->helper('url');
        $this->load->model(array('LandlordCategory_model'));
        $this->load->library('session');
        if(empty($this->session->userdata("user_id")))
        {
        	$this->session->set_flashdata("flash",["type"=>"danger","message"=>"Session out!"]);
        	redirect('AdminLogin/index');
        }
      } 
    public function index()
    {
        if (@$_SESSION['logged_in']) {
        $num_rows=$this->db->count_all("$this->table");
     	$this->load->library('pagination');

		$config['base_url'] = base_url().'index.php/LandlordCategory/index';
		$config['total_rows'] = $num_rows;
		$config['per_page'] = 5;
		
		//$config['use_page_numbers'] = TRUE;
		$config['full_tag_open'] = "<ul class='pagination'>";
		$config['full_tag_close'] ="</ul>";
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
		$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
		$config['next_tag_open'] = "<li>";
		$config['next_tagl_close'] = "</li>";
		$config['prev_tag_open'] = "<li>";
		$config['prev_tagl_close'] = "</li>";
		$config['first_tag_open'] = "<li>";
		$config['first_tagl_close'] = "</li>";
		$config['last_tag_open'] = "<li>";
		$config['last_tagl_close'] = "</li>";
		
		$this->pagination->initialize($config);
     	// ******* pagiantion configrtn ends *** /////////
         $this->db->order_by('landlord_category.id','desc');
        $query = $this->db->get("$this->table",$config['per_page'],$this->uri->segment(3)); // $config['per_page'] and $offset is for pagination
        $data['records'] = $query->result(); 
        
        $this->load->view("$this->header");
        $this->load->view("$this->baseFolder/index",$data);
        //$this->load->view('home/home_view',$data);
        $this->load->view("$this->footer");
        } else {
            redirect(base_url().'index.php/AdminLogin');
        }
    }
    public function add () {
        $this->load->helper('url');
        $this->load->library('session');
        $this->load->view('admin/header.php');
        $this->load->view('admin/landlord-category/add');
        $this->load->view('admin/footer.php');
    }
    public function create() {
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('session');
        $this->load->library('form_validation');
        $this->form_validation->set_rules('landlordCategory', 'Landlord Category', 'required');
        if ($this->form_validation->run() === FALSE)
        {
            $this->session->set_flashdata("flash", ["type" => "danger", "message" => "Please fill all the required fields!"]);
            redirect(site_url().'/LandlordCategory/add');
        } else {
            $status = $this->LandlordCategory_model->create();
            if ($status) {
                $this->session->set_flashdata("flash", ["type" => "success", "message" => "Landlord category added successfully."]);
                redirect(site_url().'/LandlordCategory/');
            } else {
                $this->session->set_flashdata("flash", ["type" => "danger", "message" => "Please fill all the required fields!"]);
                redirect(site_url().'/LandlordCategory/add');
            }
        }
    }
    public function edit($id) {
        $this->load->helper('url');
        $data['item'] = $this->LandlordCategory_model->getItemById($id);
        $this->load->view('admin/header.php', $data);
        $this->load->view('admin/landlord-category/edit');
        $this->load->view('admin/footer.php');
    }
    public function update ($id) {
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('session');
        $this->load->library('form_validation');
        $this->form_validation->set_rules('landlordCategory', 'Landlord Category', 'required');
        if ($this->form_validation->run() === FALSE)
        {
            $this->session->set_flashdata("flash", ["type" => "danger", "message" => "Please fill all the required fields!"]);
            redirect(site_url().'/LandlordCategory/edit/'.$id);
        } else {
            $status = $this->LandlordCategory_model->update_item($id);
            if ($status) {
                $this->session->set_flashdata("flash", ["type" => "success", "message" => "Landlord category details updated successfully."]);
                redirect(site_url().'/LandlordCategory/');
            } else {
                $this->session->set_flashdata("flash", ["type" => "danger", "message" => "Please fill all the required fields!"]);
                redirect(site_url().'/LandlordCategory/edit/'.$id);
            }
        }
    }
    public function delete() { 
         $id = $this->uri->segment('3'); 
         $res=$this->LandlordCategory_model->deleteData($id); 
         if($res)
         {
		 	$this->session->set_flashdata("flash", ["type" => "success", "message" => "Data deleted successfully!"]);
		 }
		 else{
		 	 $this->session->set_flashdata("flash", ["type" => "danger", "message" => "Failed to delete data!"]);
		 }
        redirect('LandlordCategory/index');   		
      }
}
