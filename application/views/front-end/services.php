<div class="page_head">
    <div class="page_head_inner">
        <div class="container">
            <h4>Our Services</h4>
        </div>
    </div>
</div>
<div class="service_wrap text-center">
    <div class="container">
        <h2 class="service_head"></h2>
        <p class="service_caption">Emirates Properties is your finest step to finding your home and to establish your business.</p>
        <p>Landlords can contact Emirates Properties directly for their Leasing Needs of their properties which can

be Residential or commercial buildings, Lands, Shops, Showrooms, Warehouses, Labor camps, Villas,

Malls etc. Landlords can choose any of the following options to lease their property though Emirates

Properties.</p>
        <!--<div class="service_inner">
            <div class="service_col">
                <div class="service_ico text-center"><i class="ion ion-android-settings"></i></div>
                <h4>Service 1</h4>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin placerat massa id mauris hendrerit, id ultricies erat gravida.</p>
            </div>
            <div class="service_col">
                <div class="service_ico text-center"><i class="ion ion-settings"></i></div>
                <h4>Service 2</h4>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin placerat massa id mauris hendrerit, id ultricies erat gravida.</p>
            </div>
            <div class="service_col">
                <div class="service_ico text-center"><i class="ion ion-clipboard"></i></div>
                <h4>Service 3</h4>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin placerat massa id mauris hendrerit, id ultricies erat gravida.</p>
            </div>
            <div class="service_col">
                <div class="service_ico text-center"><i class="ion ion-map"></i></div>
                <h4>Service 4</h4>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin placerat massa id mauris hendrerit, id ultricies erat gravida.</p>
            </div>
        </div>-->
    </div>
</div>

        <div class="service_box_inner">
                <div class="container">
                    <div class="col-lg-6 col-md-6 col-sm-6">
                        <h2>Lease and Sublease Contract:</h2>
                        <p>Lease and Sublease Contract is an agreement made between the landlord and Emirates Properties. It is a

            hassle free financial guarantee to the landlord. Emirates Properties pays the landlord for the property

            for a fixed period of time in single payment at the time of signing the agreement. Most landlords prefer

            a Lease and Sublease Contract agreement as it requires no management involvement. Emirates

            Properties will undertake the full financial risk for generating income by subleasing the property. Also

            Emirates Properties undertakes the entire facility management and the following expenses of the

            building:</p>
                        <ul>
                            <li>Building security and staff salary.</li>
                            <li>General Maintenance.</li>
                            <li>Lift maintenance.</li>
                            <li>AC maintenance.</li>
                            <li>Pest control.</li>
                            <li>Installation and maintenance of firefighting systems.</li>
                            <li>Civil defense payments.</li>
                            <li>DEWA landlord service payments.</li>
                            <li>Swimming pool maintenance, if any.</li>
                            <li>Installation and maintenance of building management systems.</li>
                            <li>Maintenance of common areas and other facilities.</li>
                            <li>Building Insurance (for new buildings).</li>
                        </ul>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6">
                        <h2>Property Management Contract:</h2>
                        <p>Property Management Contract covers complete lease management services. Emirates Properties

undertakes marketing, administration and facility management of the property on behalf of the owner.

The landlord will have to pay an annual management fee which is typically starting from 5% of the

income and goes up depending upon the size of the property to be managed. All the expenses incurred

for the building including the annual maintenance contracts for AC, Building management systems, Lift,

Swimming pool, Firefighting systems etc. and other expenses for general maintenance, staff salary, Civil

defense fees, DEWA landlord service payment, building insurance etc. will be covered from the income

of the building within the limits specified by the landlord. There will be a proper accounting for the

building income and expenses and the balance sheet will be provided to the Landlord at the end of each

year. Emirates Properties will handle all the inquiries, calls and customer relationship management

regarding the property. And Also the contract and Ejari will be provided by Emirates Properties to the

customers. But the rental and security deposit cheques will be collected in the landlord name and

cheques will be delivered to the landlord or it will be deposited to the landlord account according to his

discretion. In case any cheques are not honored, Emirates Properties will folowup for the payments and

will file case on landlord’s behalf if necessary. In this type of contract, Landlords will have full financial

control of his property and peace of mind.</p>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <h2>Standard Leasing:</h2>
                        <p>Landlord’s does not have to pay for this service. Once a new property is constructed and ready for

Leasing, Emirates Properties can undertake the entire Marketing responsibility and will rent out all the

units in the property within a stipulated time by advertising the property though both print and online

Medias. Emirates Properties will collect 5% agency fees from the tenants and will appoint our own staff

for accompanied viewing, key handover, Pre-lease inspection etc. Emirates Properties will collect the

rent and security deposit in favor of landlord and will prepare the contract between landlord and the

tenant. Once the entire property has been rented out, we will issue a letter to all the tenants informing

to contact the landlord or his representative for further communication regarding, maintenance,

renewal, termination of contract etc. In this method, Landlord’s does not have any expense and their

property will be rented out soon and they don’t have to set up their own marketing team and later can

manage the tenant affairs directly.</p>
                    </div>
                </div>
        </div>