<?php 
   class Properties_model extends CI_Model {
	protected $table='properties';
      function __construct() { 
         parent::__construct(); 
         $this->load->database();
      }
      
     public function getAllDataCat($limit,$offset,$catId){
		    $this->db->select('properties.*,property_category.*,properties.ID as prop_id,property_category.ID as cat_id');
			$this->db->from('properties');
			$this->db->join('property_category','properties.property_category_id = property_category.ID');		$this->db->where("property_category_id='$catId'");
			
			$this->db->order_by('properties.ID','desc');
            $this->db->limit($limit, $offset);
			$query = $this->db->get();
			//echo $this->db->last_query();die;
			return $query->result();
	    } 
	    public function getAllData($limit,$offset){
		    $this->db->select('properties.*,property_category.*,properties.ID as prop_id,property_category.ID as cat_id');
			$this->db->from('properties');
			$this->db->join('property_category','properties.property_category_id = property_category.ID');		
			$this->db->order_by('properties.ID','desc');
            $this->db->limit($limit, $offset);
			$query = $this->db->get();
			//echo $this->db->last_query();die;
			return $query->result();
	    } 
   	public function fetchAll() {
        $query = $this->db->get($this->table);
        return $query->result_array();
    }
   	public function insertData($params)
   	{ 
		$ins		  =	$this->db->insert($this->table,$params);
		//echo $this->db->last_query();die;
		return $ins;
	}
	
	public function getData($params)
	{
		$this->db->select($params['fields']);
		$this->db->order_by($params['order']);
		$query	=	$this->db->get_where($this->table);//, $params['condition']
		return $query->result_array();
	}
     
     public function getUpdateData($params)
     { 
     	$this->db->select($params['fields']);
		$query	=	$this->db->get_where($this->table,$params['condition']);
		return $query->result_array();		
	 }
	 public function updateAction($params,$editId)
	 {
	 	$condition=array('ID'=>$editId);
	 	$this->db->where($condition);
		$up		=	$this->db->update($this->table,$params);	
		return $up;
	 }
	  public function deleteData($id) { 
         if ($this->db->delete($this->table, "ID = ".$id)) { 
            return true; 
         } 
      } 
      public function rowWiseData($editId)
      {
	  	$query 	= $this->db->query("SELECT image_url FROM properties where id='$editId'");
		$row 	= $query->row();
		//echo $this->db->last_query();die;
		return $row;
	  }	  
}