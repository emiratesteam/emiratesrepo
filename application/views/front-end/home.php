<div class="particle_wrap">
   <!-- <img src="<?= base_url(); ?>images/banner_1.jpg" alt="">-->
    
    <!-- SLIDER STARTS -->
    <div class="automatic-slider">
        <ul class="bxslider">
            <li><img src="<?= base_url(); ?>images/banner_1.jpg" alt=""></li>
            <li><img src="<?= base_url(); ?>images/banner_2.jpg" alt=""></li>
            <li><img src="<?= base_url(); ?>images/banner_3.jpg" alt=""></li>
            <li><img src="<?= base_url(); ?>images/banner_4.jpg" alt=""></li>
        </ul>
    </div>
    
    <div class="banner_contact_wrap">
        <div class="container">
            <div class="banner_contact text-left clearfix">
                <!--<img src="images/1.png">-->
                <!--<img src="<?= base_url(); ?>images/contact_ico.png" alt="">-->
                <div class="banner_contact_right">
                    <div class="title"><span><i class="ion ion-android-call"></i></span><div class="tel"><a href="tel:+97142955588">+971 4 295 5588</a></div></div>
                    


<div class="title"><span><i class="ion ion-android-mail"></i></span><div class="tel"><a href="mailto:info@emiratesproperties.com">info@emiratesproperties.com</a></div></div>
                    

                    </div>
                </div>
                <!--<a href="tel:+97142955405"><div class="phone_link"></div></a>
                <a href="mailto:info@emirates.com"><div class="email_link"></div></a>
                <div class="fax_link"></div>-->
            </div>
        </div>
    </div>
    <div id="particles-js"></div> <!-- stats - count particles --> <div class="count-particles"> <span class="js-count-particles">--</span> particles </div>
</div>

<!-- Welcome -->
<div class="welcome">
    <div class="container">
        <div class="welcome_inner text-center">
            <h3>Welcome to Emirates Properties</h3>
            <p>Emirates properties creates a way through trust and expertise towards a perfect lease management

platform where tenants find their dream home, commercial complexes, office spaces and the property

owners showcases their finest properties. When our expertise in leasing and renting meet your needs,

property deals becomes a less tedious job for you.

Emirates Properties thus becomes an adept solution with experience and supremacy in the real estate

business over 10 years. Built upon the foundation of ethics, showcasing the sublime properties to the

world, our data base becomes an exquisite space where the finest choices of properties and buildings

across UAE are advertised.</p>
        </div>
    </div>
</div>
<div class="mission_wrap text-center">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6">
                <div class="mission_item">
                    <div class="mission_img">
                        <img class="img-responsive" src="<?= base_url(); ?>images/mission_img.jpg">
                    </div>
                    <div class="mission_desc">
                        <div class="mission_desc_inner">
                            <h4>Our Mission</h4>
                            <p>Create a legacy, by being the most trusted and unique space for customers and clients

across the world, to carry out their property deals at ease. To make the leasing and renting process a

hassle free trade by making it comfortable at every stage.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6">
                <div class="mission_item">
                    <div class="mission_img">
                        <img class="img-responsive" src="<?= base_url(); ?>images/vision_img.jpg">
                    </div>
                    <div class="mission_desc">
                        <div class="mission_desc_inner text-center">
                            <h4>Our Vision</h4>
                            <p>Create an avenue for customers to meet the best properties available across UAE partnering

with the most trusted and reputed property owners with a prime focus on customer centricity and

satisfaction.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container property_wrap text-center">
    <h3 class="text-uppercase">our properties</h3>
    <div class="property_lnk">
        <a href="<?= base_url(); ?>index.php/Properties/index/0/1">
            <img src="<?= base_url(); ?>images/property_ico_1.png" alt="" />
            <p>Residential Buildings</p>
        </a>

        <a href="<?= base_url(); ?>index.php/Properties/index/0/2">
            <img src="<?= base_url(); ?>images/property_ico_4.png" alt="" />
            <p>Commercial Buildings</p>
        </a>
        <a href="<?= base_url(); ?>index.php/Properties/index/0/3">
            <img src="<?= base_url(); ?>images/property_ico_2.png" alt="" />
            <p>Villas</p>
        </a>
        <!--<a href="#">
            <img src="<?= base_url(); ?>images/property_ico_3.png" alt="" />
            <p>Shops</p>
        </a>-->
    </div>
</div>

<div class="thinking">
    <div class="thinking_inner">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12 col-lg-offset-6 col-md-offset-6">
                    <div class="thinking_content">
                        <h3>Connect with Emirates Properties!</h3>
                        <p>To help you sell/rent and advertise your

properties</p>
                        <div class="thinking_lnk text-right">
                            <a href="<?= site_url(); ?>/landlords" class="btn btn-default">Know More</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>