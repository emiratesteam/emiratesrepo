<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Enquiries
        </h1>
    </section>
    <section class="content">
        <?php
        if ($this->session->flashdata('flash')) {
            ?>
            <div class="status status-<?= $this->session->flashdata('flash')['type']; ?>" data-role="auto-hide">
                <?= $this->session->flashdata('flash')['message']; ?>
            </div>
            <?php
        }
        ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">List of Enquiries</h3>                        
                    </div>
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Customer Name</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th>Subject</th>
                                <th>Message</th>
                                <th style="width: 200px">Action</th>                                
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            if (count($records) > 0) {
                                $i =$this->uri->segment(3);
                                foreach ($records as $enq) {
                                	$i++;
                                    ?>
                                    <tr>
                                        <td><?= $i ?></td>
                                        <td><?= $enq->name; ?></td>
                                        <td><?= $enq->email; ?></td>
                                        <td><?= $enq->phone; ?></td>
                                        <td><?= $enq->subject; ?></td>
                                        <td><?= $enq->message; ?></td> 
                                        <td> <a href="<?php echo site_url(); ?>/Contact/delete/<?= $enq->id; ?>" onclick="return delete_type()" class="btn btn-danger btn-flat" onclick="return delete_type()">Delete</a> 
                                        </td>                                        
                                    </tr>
                                    <?php                                    
                                }
                            } else {
                                ?>
                                <tr><td colspan="6" align="center">No records found.</td></tr>
                                <?php
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <?php 
                  	$rowCount = count($records);
                  	 ?>
                  	<!-- for pagination --->
					<div class="row" align="center">
						<?php if($rowCount!=0) { echo $this->pagination->create_links();} else {}; ?>
					</div>
					<!-- pagination end -->
            </div>
        </div>
    </section>
</div>

<!-- /.content-wrapper -->
