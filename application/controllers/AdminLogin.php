<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * User class.
 *
 * @extends CI_Controller
 */
class AdminLogin extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->model('login_model');
        $this->load->helper('url');
        $this->load->library('form_validation');
    }
    public function index () {        
            $this->load->helper('form');
            $this->load->library('form_validation');
            $this->load->view('admin/index');        
    }
    public function login() {
        // create the data object
        $data = new stdClass();
        
            // load form helper and validation library
            $this->load->helper('form');
            $this->load->library('form_validation');

            // set validation rules
            $this->form_validation->set_rules('userName', 'Username', 'required|alpha_numeric');
            $this->form_validation->set_rules('password', 'Password', 'required');

            if ($this->form_validation->run() == false) {

                // validation not ok, send validation errors to the view
                redirect('AdminLogin');

            } else {

                // set variables from the form
                $userName = $this->input->post('userName');
                $password = $this->input->post('password');
                if ($this->login_model->resolve_user_login($userName, $password)) {

                    $user_id = $this->login_model->get_user_id_from_username($userName);
                    $user    = $this->login_model->get_user($user_id);

                    // set session user datas
                    $_SESSION['user_id']      = (int)$user->id;
                    $_SESSION['username']     = (string)$user->userName;
                    $_SESSION['logged_in']    = (bool)true;
                    $_SESSION['is_confirmed'] = (bool)true;
                    $_SESSION['user_type']     = (string)$user->userType;

                    redirect('AdminDashboard');                   

                } else {
						 $this->session->set_flashdata("flash", ["type" => "danger", "message" => "Wrong User Name or password!"]);                   
                    redirect('AdminLogin');
                }

            }       
    }
    public function logout() {

        // create the data object
        $data = new stdClass();
        $this->load->helper('form');
        $this->load->library('form_validation');

        if (isset($_SESSION['logged_in']) && $_SESSION['logged_in'] === true) {

            // remove session datas
            foreach ($_SESSION as $key => $value) {
                unset($_SESSION[$key]);
            }

            // user logout ok
            redirect('AdminLogin');

        } else {

            // there user was not logged in, we cannot logged him out,
            // redirect him to site root
            //$this->load->view('index', $data);
            redirect('AdminLogin');
        }

    }
    public function changePasswordForm () {
        if (@$_SESSION['logged_in']) {
            $this->load->helper('form');
            $this->load->library('form_validation');
            $this->load->view('admin/header');
            $this->load->view('admin/change-password/index.php');
            $this->load->view('admin/footer');
        } else {
            redirect('AdminLogin');
        }
    }
    public function changePassword () {
        $this->load->helper('form');
        $this->load->library('form_validation');

        // set validation rules
        $this->form_validation->set_rules('password', 'Password', 'required');
        $this->form_validation->set_rules('nPassword', 'New Password', 'required');
        $this->form_validation->set_rules('cPassword', 'Confirm Password', 'required');
        if ($this->form_validation->run() == false) {
            // validation not ok, send validation errors to the view
            $this->load->view('admin/header');
            $this->load->view('admin/change-password/index');
            $this->load->view('admin/footer');
        } else {
            $currentPassword = $this->input->post('password');
            $newPassword = $this->input->post('nPassword');
            $confirmPassword = $this->input->post('cPassword');
            if ($newPassword != $confirmPassword) {
                $this->session->set_flashdata("flash", ["type" => "danger", "message" => "Password mismatch!"]);
                $this->load->view('admin/header');
                $this->load->view('admin/change-password/index');
                $this->load->view('admin/footer');
            } else {
                $user = $this->login_model->get_user($_SESSION['user_id']);
                if ($this->login_model->checkPasswordMatch($currentPassword, $user->password)) {
                    if ($this->login_model->updatePassword($_SESSION['user_id'], $newPassword)) {
                        redirect('AdminLogin/logout');
                    } else {
                        $this->session->set_flashdata("flash", ["type" => "danger", "message" => "Something went wrong! Please try again."]);
                        $this->load->view('admin/header');
                        $this->load->view('admin/change-password/index');
                        $this->load->view('admin/footer');
                    }
                } else {
                    $this->session->set_flashdata("flash", ["type" => "danger", "message" => "Current password is incorrect!"]);
                    $this->load->view('admin/header');
                    $this->load->view('admin/change-password/index');
                    $this->load->view('admin/footer');
                }
            }
        }
    }
}